terraform {
#  backend "consul" {
#    address = ""
#    scheme  = "http"
#    path    = "dns/terraform.tfstate"
#    lock    = true
#  }

  backend "local" {
    path    = "init/terraform.tfstate"
  }

  required_providers {
    dns = {
      source  = "hashicorp/dns"
      version = "3.2.1"
    }
  }

}