resource "dns_a_record_set" "a_1409" {
  zone      = var.zone
  name      = "1409"
  addresses = [
    "192.168.0.1"
  ]
  ttl       = 300
}
